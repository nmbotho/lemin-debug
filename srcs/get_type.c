/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_type.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 17:59:05 by nmbotho           #+#    #+#             */
/*   Updated: 2017/09/27 18:01:42 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../lemin.h"

int		ft_return(int code, char ***values)
{
	ft_free2dsafe((void***)values);
	return (code);
}

int		get_type(char *line)
{
	int		i;
	int		j;
	char	**values;

	i = 0;
	j = 0;
	if (!(values = ft_split(line)))
		return (-1);
	if (values[0][0] == '#')
		return (ft_return(COMMENT, &values));
	else if (ft_2dcount(values) == 1 && ft_isalldigit(values[0]))
		return (ft_return(ANTCOUNT, &values));
	else if (ft_2dcount(values) == 1)
	{
		while (values[i][j] != '-')
			j++;
		if (values[i][j] == '-')
			return (ft_return(LINK, &values));
	}
	else if (ft_2dcount(values) == 3)
	{
		if (ft_isalldigit(values[1]) && ft_isalldigit(values[2]))
			return (ft_return(ROOM, &values));
	}
	return (ft_return(INVALID, &values));
}
