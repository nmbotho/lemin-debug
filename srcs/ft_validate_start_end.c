/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_validate_start_end.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/18 11:22:56 by nmbotho           #+#    #+#             */
/*   Updated: 2018/01/18 11:23:09 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../lemin.h"
#define ROOM_IS_START rooms[i]->status == START
#define ROOM_IS_END rooms[i]->status == END
#define LINE_IS_START ft_strcmp(map[i], "##start") == 0
#define LINE_IS_END ft_strcmp(map[i], "##end") == 0

int			ft_validate_start_end(char **map)
{
	int		i;
	char	**values;

	i = -1;
	values = NULL;
	while (map[++i])
	{
		if (LINE_IS_START || LINE_IS_END)
		{
			values = ft_strsplit(map[i + 1], ' ');
			if (ft_2dcount(values) != 3)
			{
				ft_free2dsafe((void***)&values);
				ft_putendl("Invalid START or END");
				return (0);
			}
			else if (!ft_isalldigit(values[1]) || !ft_isalldigit(values[2]))
			{
				ft_free2dsafe((void***)&values);
				ft_putendl("Invalid START or END");
				return (0);
			}
		}
	}
	return (1);
}

void		ft_check_solvability(t_room **rooms, t_ant ***ants)
{
	int i;

	i = 0;
	while (rooms[i])
	{
		if ((ROOM_IS_START || ROOM_IS_END) && rooms[i]->links[0] == NULL)
		{
			ft_putendl("Map is aready in solved state\n");
			ft_freeants(ants);
			ft_freerooms(rooms);
			exit(0);
		}
		i++;
	}
}
