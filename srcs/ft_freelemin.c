/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freelemin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/27 17:18:46 by nmbotho           #+#    #+#             */
/*   Updated: 2017/09/27 17:18:52 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../lemin.h"

void		ft_free2dsafee(void ***arr)
{
	int		i;
	void	**array;

	i = 0;
	array = *arr;
	if (array)
	{
		while (array[i])
		{
			free(array[i]);
			array[i] = NULL;
			i++;
		}
		free(*arr);
		*arr = NULL;
	}
}

void		ft_freerooms(t_room **rooms)
{
	int i;

	i = 0;
	while (rooms[i])
	{
		free(rooms[i]->name);
		free(rooms[i]->links);
		free(rooms[i]->ants);
		rooms[i]->name = NULL;
		rooms[i]->links = NULL;
		rooms[i]->ants = NULL;
		free(rooms[i]);
		rooms[i] = NULL;
		i++;
	}
	free(rooms);
	rooms = NULL;
}

void		ft_freeants(t_ant ***all_ants)
{
	ft_free2dsafee((void***)all_ants);
	*all_ants = NULL;
}
