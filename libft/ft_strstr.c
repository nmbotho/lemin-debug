/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/02 13:06:38 by nmbotho           #+#    #+#             */
/*   Updated: 2017/07/29 16:53:10 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strstr(const char *big, const char *little)
{
	int	len;

	len = ft_strlen(little);
	if (!*big && !*little)
		return ((char*)big);
	while (*big)
	{
		if (*big == *little || len == 0)
			if (ft_strncmp(big, little, len) == 0)
				return ((char *)big);
		big++;
	}
	return (NULL);
}
