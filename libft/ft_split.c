/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/29 16:41:47 by nmbotho           #+#    #+#             */
/*   Updated: 2017/09/02 11:28:05 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int	is_space(int c)
{
	if ((c >= 7 && c <= 13) || c == 32)
		return (1);
	return (0);
}

t_string	*ft_split(t_string s)
{
	t_list	*letters;
	t_list	*words;
	int		i;
	int		count;

	if (s)
	{
		i = -1;
		letters = NULL;
		words = NULL;
		count = ft_strlen(s) - 1;
		while (++i <= count || letters)
		{
			if (is_space(s[i]) == 0 && i <= count)
				ft_lstadd_last_d(&letters, ft_strdup((s + i)), 1);
			else if (letters != NULL)
				ft_lstadd_last_d(&words, ft_lst_to_string(&letters), 1);
		}
		return (ft_lst_to_2dstring(&words));
	}
	return (NULL);
}
