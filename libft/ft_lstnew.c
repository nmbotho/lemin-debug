/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/29 16:26:13 by nmbotho           #+#    #+#             */
/*   Updated: 2017/07/29 16:27:17 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*link;
	size_t	size;

	size = content_size;
	link = (t_list*)malloc(sizeof(t_list));
	if (link)
	{
		if (content != NULL)
		{
			if (!(link->content = ft_memalloc(size + 1)))
				return (NULL);
			link->content = ft_memcpy(link->content, (void*)content, size);
			link->content_size = content_size;
		}
		else
		{
			link->content_size = 0;
			link->content = NULL;
		}
		link->next = NULL;
	}
	return (link);
}
