/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_free.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/29 15:36:40 by nmbotho           #+#    #+#             */
/*   Updated: 2017/09/02 11:32:38 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lst_free(t_list **list)
{
	t_list	*next;
	t_list	*current;

	current = *list;
	while (current)
	{
		next = current->next;
		free(current->content);
		current->content = NULL;
		free(current);
		current = next;
	}
	current = NULL;
}
