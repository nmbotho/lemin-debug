/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/22 10:07:52 by nmbotho           #+#    #+#             */
/*   Updated: 2017/07/31 13:04:05 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include "../libft.h"
# define BUFF_SIZE 7

typedef	char*	t_string;
typedef	struct	s_file
{
	int			fd;
	char		buffer[BUFF_SIZE];
	int			used;
}				t_file;
int				get_next_line(const int fd, t_string *line);
int				ft_readbuffer(int fd, char **line, t_list **list, char *buf);
int				ft_readfile(int fd, char **line, t_list **list, char *buf);
#endif
