/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/16 10:23:15 by nmbotho           #+#    #+#             */
/*   Updated: 2017/06/25 18:25:18 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/get_next_line.h"

int			get_file(int fd, t_file *files)
{
	int i;

	i = 0;
	while (files[i].used)
	{
		if (files[i].fd == fd)
			return (i);
		i++;
	}
	files[i].used = 1;
	files[i].fd = fd;
	return (i);
}

void		ft_init_files(t_file *files)
{
	int i;

	i = 0;
	while (i < 50)
	{
		files[i].used = 0;
		files[i].fd = 0;
		ft_bzero((files + i)->buffer, BUFF_SIZE);
		i++;
	}
}

int			get_next_line(int fd, t_string *line)
{
	static t_file	files[50];
	t_file			*file;
	int				result;
	t_list			*char_list;

	if ((*files).used == 0)
		ft_init_files(files);
	file = &files[get_file(fd, files)];
	char_list = NULL;
	result = 0;
	*line = NULL;
	if (BUFF_SIZE < 1)
		return (-1);
	if ((file->fd < 0) || (file->fd == 99) || (file->fd == 1))
		return (-1);
	if (!line)
		return (1);
	if (*(file->buffer))
		result = ft_readbuffer(file->fd, line, &char_list, file->buffer);
	else
		result = ft_readfile(file->fd, line, &char_list, file->buffer);
	return (result);
}

int			ft_readfile(int fd, char **line, t_list **char_list, char *buffer)
{
	int ret;

	ret = 0;
	while ((ret = read(fd, buffer, BUFF_SIZE)))
	{
		if (ret < 1)
			return (ret);
		while (*buffer)
		{
			if (*buffer == '\n')
			{
				ft_memmove(buffer, buffer + 1, ft_strlen(buffer));
				*line = ft_lst_to_string(char_list);
				return (1);
			}
			ft_lstadd_last(char_list, ft_lstnew(buffer, 1));
			ft_memmove(buffer, buffer + 1, ft_strlen(buffer));
		}
	}
	if (ret == 0 && buffer[0] == '\0' && ft_lst_count(*char_list) > 0)
	{
		*line = ft_lst_to_string(char_list);
		return (1);
	}
	return (0);
}

int			ft_readbuffer(int fd, char **line, t_list **char_list, char *buffer)
{
	while (*buffer)
	{
		if (*buffer == '\n')
		{
			ft_memmove(buffer, buffer + 1, ft_strlen(buffer));
			*line = ft_lst_to_string(char_list);
			return (1);
		}
		ft_lstadd_last(char_list, ft_lstnew(buffer, 1));
		ft_memmove(buffer, buffer + 1, ft_strlen(buffer));
	}
	return (ft_readfile(fd, line, char_list, buffer));
}
