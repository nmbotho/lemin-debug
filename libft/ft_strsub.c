/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmbotho <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 14:12:14 by nmbotho           #+#    #+#             */
/*   Updated: 2017/07/29 16:56:30 by nmbotho          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(const char *s, unsigned int start, size_t len)
{
	char	*substr;
	size_t	i;
	size_t	k;

	if (!s)
		return (NULL);
	substr = (t_string)malloc(sizeof(char) * (len + 1));
	i = start;
	k = 0;
	if (!substr)
		return (NULL);
	if (len)
	{
		while (i <= (start + len) - 1)
		{
			substr[k] = s[i];
			i++;
			k++;
		}
	}
	substr[k] = '\0';
	return (substr);
}
